<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_crmetm
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the login functions only once
require_once __DIR__ . '/helper.php';

$layout    = $params->get('layout', 'default');
require JModuleHelper::getLayoutPath('mod_crmetm', $layout);
